package dinner

import "math/rand"

type Dish string

type Dinner struct {
	dishes  []Dish
	morsels map[Dish]int
}

func New(dishes []Dish) *Dinner {
	morsels := make(map[Dish]int)
	for _, dish := range dishes {
		morsels[dish] = 0
	}

	return &Dinner{
		dishes:  dishes,
		morsels: morsels,
	}
}

func NewRandomDinner(dishes []Dish, min, max int) *Dinner {
	d := New(dishes)
	for _, dish := range dishes {
		d.morsels[dish] = rand.Intn(max - min) + min
	}
	return d
}

func (d *Dinner) Serve(c chan Dish) {
	go func() {
		for d.HasMorsels() {
			dish, _ := d.RandomDish()
			c <- dish
		}
		close(c)
	}()
}

func (d *Dinner) HasMorsels() bool {
	for _, n := range d.morsels {
		if n > 0 {
			return true
		}
	}
	return false
}

func (d *Dinner) RandomDish() (Dish, bool) {
	n := len(d.dishes)
	if n == 0 {
		return "", false
	}

	dish := d.dishes[rand.Intn(n)]
	if d.morsels[dish] > 0 {
		d.morsels[dish]--
		return dish, true
	}

	delete(d.morsels, dish)
	return d.RandomDish()
}
