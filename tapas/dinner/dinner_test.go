package dinner

import (
	"testing"
)

func TestNewDinnerHasNoMorsels(t *testing.T) {
	d := New([]Dish{"banana", "potato"})
	if d.HasMorsels() {
		t.Errorf("New Dinner should be empty")
	}
}
