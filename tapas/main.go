package main

import (
	"fmt"
	"gitlab.com/allanmatheus/go-concurrency/tapas/dinner"
	"gitlab.com/allanmatheus/go-concurrency/tapas/person"
	"sync"
)

const (
	MIN_MORSELS = 5
	MAX_MORSELS = 10
)

var FRIENDS = [...]string{"Alice", "Bob", "Charlie", "Dave"}
var DISHES = [...]dinner.Dish{
	"chorizo", "pimientos  de padrón", "croquetas", "patatas bravas",
	"chopitos",
}

func main() {
	n := len(FRIENDS)
	friends := make([]*person.Person, n)
	for i, name := range FRIENDS {
		friends[i] = person.New(name)
	}

	tapasDinner := dinner.NewRandomDinner(DISHES[:],
		MIN_MORSELS, MAX_MORSELS+1)

	dishes := make(chan dinner.Dish, n)
	tapasDinner.Serve(dishes)

	fmt.Println("Bon appétit!")
	var wg sync.WaitGroup
	for _, p := range friends {
		wg.Add(1)
		p.Eat(dishes, &wg)
	}

	wg.Wait()
	fmt.Println("That was delicious!")
}
