package person

import (
	"fmt"
	"gitlab.com/allanmatheus/go-concurrency/tapas/dinner"
	"math/rand"
	"sync"
	"time"
)

const (
	MIN_DELAY = 3
	MAX_DELAY = 18
)

type Person struct {
	name string
}

func New(name string) *Person {
	return &Person{name: name}
}

func (p *Person) showDish(d dinner.Dish) {
	fmt.Println(p.name, "is enjoying some", d)
}

func (p *Person) waitDelay() {
	n := rand.Intn(MAX_DELAY-MIN_DELAY+1) - MIN_DELAY
	d := time.Duration(n) * time.Second
	time.Sleep(d)
}

func (p *Person) Eat(dishes <-chan dinner.Dish, wg *sync.WaitGroup) {
	go func() {
		for dish := range dishes {
			p.showDish(dish)
			p.waitDelay()
		}
		wg.Done()
	}()
}
