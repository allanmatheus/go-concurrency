package main

import (
	"fmt"
	"sync"
	"time"
)

type Runner struct {
	Name string
}

func (r *Runner) LetsWalk(o *Runner) {
	fmt.Println("Let's go for a walk!")

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()
		o.GetReady()
	}()

	go func() {
		defer wg.Done()
		r.GetReady()
	}()

	wg.Wait()

	wg.Add(1)

	go func() {
		defer wg.Done()
		SetAlarm()
	}()

	var subWg sync.WaitGroup
	subWg.Add(2)

	go func() {
		defer subWg.Done()
		o.PutShoes()
	}()

	go func() {
		defer subWg.Done()
		r.PutShoes()
	}()

	subWg.Wait()

	fmt.Println("Exiting and locking door.")

	wg.Wait()
}

func (r *Runner) GetReady() {
	fmt.Println(r.Name, "started getting ready")

	delay := SpendSomeTime(60, 90, time.Second)
	fmt.Println(r.Name, "spent", delay, "getting ready")
}

func (r *Runner) PutShoes() {
	fmt.Println(r.Name, "started putting on shoes")

	delay := SpendSomeTime(35, 45, time.Second)
	fmt.Println(r.Name, "spent", delay, "putting on shoes")
}
