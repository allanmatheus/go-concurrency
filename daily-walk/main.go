package main

import (
	"math/rand"
	"time"
)

func main() {
	alice := &Runner{"Alice"}
	bob := &Runner{"Bob"}

	alice.LetsWalk(bob)
}

func init() {
	rand.Seed(time.Now().Unix())
}
