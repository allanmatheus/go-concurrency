package main

import (
	"fmt"
	"math/rand"
	"time"
)

func SpendSomeTime(min, max int, unit time.Duration) time.Duration {
	x := rand.Intn(max)
	if x < min {
		x = min
	}
	delay := time.Duration(x) * unit
	time.Sleep(delay)

	return delay
}

func SetAlarm() {
	fmt.Println("Arming alarm")
	SpendSomeTime(30, 30, time.Second)

	fmt.Println("Alarm is counting down")
	SpendSomeTime(30, 30, time.Second)

	fmt.Println("Alarm is armed")
}
